#######MODULOS BASICOS######
import time, os
import datetime
from datetime import date, timedelta
import pandas
import yaml
import numpy as np

########MODULOS AUTOMAÇÂO WEB#########

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import ElementClickInterceptedException
#######MODULOS PARA INTERAGIR COM O BACOS###############
import mysql.connector
import difflib

#######FUNÇÃO PARA CARREGAR CONFIGURAÇÕES###############
def load_yaml():
    with open('config_BCKH.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data

#######FUNÇÕES DEFINIÇÃO DE TEMPO###############
def prepara_tempo(args):
    select_time = {
        'relative': None,
        'specific_to_date': None,
        'specific_to_time': None
    }
    try:
        if args.SelectTime:
            if args.SelectTime[0] == 'relative':
                print('relative')
                coisa = int(args.SelectTime[1])
                select_time[args.SelectTime[0]] = coisa
            elif args.SelectTime[0] == 'specific_to_date':
                print('specific_to_date')
                if  args.SelectTime[1] == 'daysago':
                    temp_date=(datetime.date.today()-datetime.timedelta(days=int(args.SelectTime[2]))).strftime("%Y-%m-%d")
                    start_time=temp_date
                    end_time=temp_date
                else:
                    start_time=args.SelectTime[1]
                    end_time=args.SelectTime[2]
                coisa = (start_time, end_time)
                print(coisa)
                select_time[args.SelectTime[0]] = coisa
            elif args.SelectTime[0] == 'specific_to_time':
                start_date=args.SelectTime[1]
                end_date=args.SelectTime[2]
                coisa = (start_date, end_date)
                select_time[args.SelectTime[0]] = coisa
            else:
                print('ta errado irmao!')
                exit()
        else:
            print('Utilizando intervalo de tempo relativo padrão: 4h')
            select_time = {
                'relative': 24,
                'specific_to_date': None,
                'specific_to_time': None
            }
    except (ValueError, IndexError) as e:
        print('Ajeita esse argumento aí!')
        exit()
    finally:
        print(select_time)
        return select_time

def seleciona_tempo(time_select):

    if time_select['specific_to_date']:
        start_time=datetime.datetime.strptime(time_select['specific_to_date'][0], '%Y-%m-%d')
        end_time=datetime.datetime.strptime(time_select['specific_to_date'][1], '%Y-%m-%d')

    if time_select['specific_to_time']:
        start_time=datetime.datetime.strptime(time_select['specific_to_time'][0].replace('_',' '), '%Y-%m-%d %H:%M:%S')
        end_time=datetime.datetime.strptime(time_select['specific_to_time'][1].replace('_',' '), '%Y-%m-%d %H:%M:%S')

    if time_select['relative']:
        end_time = datetime.datetime.now()
        start_time = datetime.datetime.combine(date.today(), datetime.time(datetime.datetime.now().hour,datetime.datetime.now().minute)) - timedelta(hours=int(time_select['relative']))
    return (start_time, end_time)


#######################FUNÇÔES DE AUTOMAÇÂO COM SELENIUM########################
def espera_clica(xpath,driver):
    timeout=20
    while True:
        try:
            wait = WebDriverWait(driver,timeout)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, xpath)))
            element.click()
            print('achou '+ xpath)
            break
        except ElementClickInterceptedException:
            time.sleep(1)
            print('esperando '+ xpath +' aparecer...')

def espera(xpath,driver):
    c=0
    print('esperando...')
    while c<20:
        try:
            wait = WebDriverWait(driver,200)
            quit_queryperformance = wait.until(EC.visibility_of_element_located((By.XPATH, xpath)))
            print('achou '+ xpath)
            break
        except:
            time.sleep(1)
            c+=1
            print('esperando '+ xpath +' aparecer...')


# def clica_ate_deixar_de_aparecer(xpath):
#     print('esperando...')
#     while True:
#         try:
#             time.sleep(0.5)
#             espera_clica(xpath)
#             print('clicou '+ xpath)
#         except:
#             print('parou de clicar '+ xpath )
#             break

def espera_download(path_to_downloads):
    seconds = 0
    dl_wait = True
    while dl_wait and seconds < 1800:
        time.sleep(1)
        print(seconds)
        dl_wait = False
        for fname in os.listdir(path_to_downloads):
            if fname.endswith('.crdownload'):
                dl_wait = True
        seconds += 1
    print('Download terminado!')
    #send_frase('Download terminado!')
    return seconds

def espera_download_comecar(path_to_downloads):
    print('esperando download começar...')
    seconds = 0
    while len(os.listdir(path_to_downloads))==0:
        time.sleep(5)
        print(seconds)
        seconds += 5
        pass
    print('download iniciado...')


def apaga_arquivos(lista_arquivos,path_to_downloads):
    for arquivo in lista_arquivos:
        os.remove(path_to_downloads+lista_arquivos[arquivo])
        print('Arquivo '+lista_arquivos[arquivo]+' removido!')


def seleciona_option(id,option,driver):
    print('esperando...')
    xpath = '//select[@id="'+id+'"]/option[contains(text(), "'+option+'")]'
    while True:
        try:
            wait = WebDriverWait(driver,200)
            element = wait.until(EC.visibility_of_element_located((By.XPATH, xpath)))
            element = wait.until(EC.element_to_be_clickable((By.XPATH, xpath)))
            print('achou '+ xpath)
            break
        except:
            time.sleep(1)
            print('esperando '+ xpath +' aparecer...')
    print('achou')
    select = Select(driver.find_element(By.XPATH,'//*/select[@id="'+str(id)+'"]'))
    select.select_by_visible_text(str(option))

def retorna_options(id,driver):
    espera('//select[@id="'+id+'"]',driver)
    select = Select(driver.find_element(By.XPATH,'//select[@id="'+str(id)+'"]'))
    return select.options

def seleciona_lista_options(id,options,driver):
    for option in options:
        espera('//select[@id="'+id+'"]/option[contains(text(), "'+option+'")]')
        select = Select(driver.find_element(By.XPATH,'//select[@id="'+str(id)+'"]'))
        select.select_by_visible_text(str(option))

def clica_lista_elem(id,lista,driver):
    for option in lista:
        espera_clica('//select[@id="'+id+'"]/option[contains(text(), "'+option+'")]',driver)

def seleciona_todas_options(id,driver):
    for option in retorna_options(id,driver):
        seleciona_option(id,str(option.text),driver)

def escreve_datas(data_inicio, data_fim,driver):
    elem_ini = driver.find_element(By.XPATH,'//*[@id="data_inicio"]')
    elem_ini.clear()
    elem_ini.send_keys(data_inicio, Keys.RETURN)

    elem_fim = driver.find_element(By.XPATH,'//*[@id="data_fim"]')
    elem_fim.clear()
    elem_fim.send_keys(data_fim, Keys.RETURN)

def seleciona_existente(id, valor,driver):
    if valor:
        if valor==True:
            seleciona_todas_options(id,driver)
        elif type(valor)==str:
            seleciona_option(id, valor,driver)
        elif type(valor)==list:
            #seleciona_lista_options(id,valor)
            clica_lista_elem(id,valor,driver)
    else:
        pass

###########################FUNÇÔES PARA INSERT NO BACOS##########################
def audit_campos(tablename,cursor):
    ultima_data= None
    query = """DESCRIBE npm."""+tablename
    cursor.execute(query)
    records = cursor.fetchall()
    lista_de_campos = []
    for row in records:
        lista_de_campos.append(row[0])
    now = datetime.datetime.now()
    query = "SELECT distinct DATA FROM npm."+tablename #+ " WHERE year(Time)="+str(now.year)#+" and month(Time)="+str(now.month) #+" and month(Time)="+str(now.month-1)
    cursor.execute(query)
    records = cursor.fetchall()
    for row in records:
        ultima_data = row[0]
    if type(ultima_data)==str:
        if 'week' in tablename:
            ultima_data = datetime.datetime.strptime(ultima_data+'-0', "%Y-%W-%w")

    return lista_de_campos, ultima_data

def lista_col_csv(dataframe_ori):
    lista_cols = []
    for (columnName, columnData) in dataframe_ori.iteritems():
        lista_cols.append(columnName)
    return lista_cols

def string_equivalente(columnName, lista_de_campos_DB, lista_de_cols_csv):
    col_compativel_BD_list = difflib.get_close_matches(columnName, lista_de_campos_DB)
    if len(col_compativel_BD_list)!=0:
        col_compativel_BD = col_compativel_BD_list[0]
        col_compativel_csv = difflib.get_close_matches(col_compativel_BD, lista_de_cols_csv)[0]

        if columnName == col_compativel_csv:
            columnName = col_compativel_BD
            #print(columnName, col_compativel_BD)
        else:
            columnName = None
            #print("NONE MATCHES")
    else:
        columnName = None
        #print("NONE MATCHES")
    return columnName


def insertCSVIntoTable(dataframe_ori,table_name,path_to_downloads,NE,args):

    path_to_csv = path_to_downloads+table_name+'.csv'

    try:
        connection = mysql.connector.connect(

            host="10.26.82.160",
            user="rodsm",
            passwd="huawei1234",
            database="npm",
            allow_local_infile=True
        )
        cursor = connection.cursor()
        print("\nConectado ao Bacos!")

        dataframe_ori['DATA'] = pandas.to_datetime(dataframe_ori['DATA'], format='%d/%m/%Y %H:%M')
        if '_4g_' in table_name:
        #formatando coluna cidade
            dataframe_ori['MUNICIPIO'] = dataframe_ori['MUNICIPIO'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
            dataframe_ori['MUNICIPIO'] = dataframe_ori['MUNICIPIO'].str.replace(' ', '_', regex=True)
            dataframe_ori['MUNICIPIO'] = dataframe_ori['MUNICIPIO'].str.replace("'", '', regex=True)
            dataframe_ori['MUNICIPIO'] = dataframe_ori['MUNICIPIO'].str.upper()
        dataframe_ori.fillna('', inplace = True)
        dataframe_ori = dataframe_ori.where(pandas.notnull(dataframe_ori), None)

        lista_de_campos, ultima_data = audit_campos(table_name, cursor)
        lista_de_colunas_csv = lista_col_csv(dataframe_ori)

        string_de_colunas = ''
        lista_de_colunas_compativeis_nos_arquivos = []
        lista_de_colunas_inexistentes_no_BD=[]
        for columnName in lista_de_colunas_csv:
            #dict_colunas[columnName] = columnData.values #atribui a lista de valores em cada coluna
            #faz string das colunas
            lista_col_eq = string_equivalente(columnName, lista_de_campos, lista_de_colunas_csv)
            if lista_col_eq:
                lista_de_colunas_compativeis_nos_arquivos.append(columnName)
                columnName = lista_col_eq
                if '(' in columnName or ' ' in columnName:
                    columnName = columnName.strip(' ')
                    columnName = '`' + columnName + '`'
                string_de_colunas += columnName +','
            else:
                lista_de_colunas_inexistentes_no_BD.append(columnName)

        string_de_colunas=string_de_colunas.strip(',')
        lista_de_colunas=string_de_colunas.split(',')

        #tratando valores
        for col in lista_de_colunas_compativeis_nos_arquivos:
            if col == 'Time' or col in NE:
                pass
            else:
                dataframe_ori[col] = pandas.to_numeric(dataframe_ori[col], errors='coerce',downcast='float')
                dataframe_ori = dataframe_ori.replace(np.nan, 0, regex=True)

        dataframe_ori.to_csv(path_to_csv, index = False, header=True)


        if len(lista_de_colunas)!=len(lista_de_campos):
            print("O número de cols na tabela do Bacos e do download estão diferentes!")
            print("As colunas inexistentes no Banco são: ", lista_de_colunas_inexistentes_no_BD)

        subs_var = ['@col'+str(x+1) for x in range(len(string_de_colunas.split(',')))]
        subs_var_string = ""
        for c in subs_var:
            subs_var_string+=c+','
        subs_var_string=subs_var_string.strip(',')
        set_cols = 'SET '
        for sub_var, col in zip(subs_var, string_de_colunas.split(',')):
            if sub_var == '@col4':
                print('Aplicando provisoriamente corte no nome dos sites (varchar(15))')
                set_cols += col+"=left(nullif("+sub_var+",'-'),15),\n"
            else:
                set_cols += col+"=nullif("+sub_var+",''),\n"

        set_cols = set_cols.strip(',\n')+";"
        print("Compondo query...")
        mySql_insert_query = f'LOAD DATA LOCAL INFILE "@@path_to_csv@@" REPLACE INTO TABLE @@table@@\n' \
                             f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                             f'LINES TERMINATED BY \'\\r\\n\' ignore 1 lines \n' \
                             f'(@@subs_var_string@@) \n' \
                             f'@@SET@@'
        # mySql_insert_query = """
        # LOAD DATA LOCAL INFILE '@@path_to_csv@@'
        # REPLACE INTO TABLE @@table@@
        # FIELDS TERMINATED BY ','
        # LINES TERMINATED BY '\\r\\n'
        # IGNORE 1 LINES
        # (@@subs_var_string@@)
        # @@SET@@
        # """

        #if args.ignore:
        #    mySql_insert_query = mySql_insert_query.replace('REPLACE','IGNORE')

        mySql_insert_query = mySql_insert_query.replace('@@path_to_csv@@',path_to_csv.replace('\\','/'))
        mySql_insert_query = mySql_insert_query.replace('@@table@@',table_name)
        mySql_insert_query = mySql_insert_query.replace('@@subs_var_string@@', subs_var_string)
        mySql_insert_query = mySql_insert_query.replace('@@SET@@', set_cols)

        if not args.justdownload:
            print("Executando o seguinte SQL: ",mySql_insert_query)

            cursor.execute(mySql_insert_query)
            connection.commit()
            print('upload concluído!')

    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

###############################################RUN SQL SCRIPTS#######################################################
def conecta_mariadb():
    con_mariadb = mysql.connector.connect(
        host="10.26.82.160",
        user="rodsm",
        passwd="huawei1234",
        allow_local_infile=True
    )
    return con_mariadb
def executeScriptsFromFile(filename):
    #DEFINE CONEXÃO DB
    con_bacos = conecta_mariadb()
    cursor = con_bacos.cursor()
    # Open and read the file as a single buffer
    fd = open(filename, 'r')
    sqlFile = fd.read()
    fd.close()

    # all SQL commands (split on ';')
    sqlCommands = sqlFile.split(';')

    # Execute every command from the input file
    for command in sqlCommands:
        # This will skip and report errors
        # For example, if the tables do not yet exist, this will skip over
        # the DROP TABLE commands
        try:
            cursor.execute(command)
            con_bacos.commit()
        except Exception as e:
            print(f'Command skipped: {e}')
def runSQLScripts(scripts):
    basepath = os.path.dirname(__file__)
    script_path = os.path.abspath(os.path.join(basepath, 'scripts'))
    all_query_files = os.listdir(script_path)
    print(f'Script Runner Start: {scripts}')
    for query in all_query_files:
        if query.split('.sql')[0] in scripts:
            actual_query = query.replace('.sql','')
            actual_query_file = os.path.join(script_path, query)
            print(f'Runnig: {actual_query}')
            executeScriptsFromFile(actual_query_file)
        else:
            continue

    return True