use npm;

set @thrh=0.5;
set @thrpl=0.1;
set @thrd=20;
set @thrpdv=8;
set @dago=35;

replace into
	npm.tx_4g_offenders_fup2
select
	datex,
	uf,
	if(cn is null,'NA',cn) as cn,
	if(city is null,'NA',city) as city,
	if(cluster is null,'NA',cluster) as cluster,
    count(site) sites_qty,
    sum(pkt_loss_down_samples) as pkt_loss_down_samples,
	sum(if(perc_pkt_loss_down_at>=@thrh,1,0)) as pkt_loss_down_sat,
    sum(if(perc_pkt_loss_down_at<@thrh,1,0)) as pkt_loss_down_sbt,
    sum(pkt_loss_up_samples) as pkt_loss_up_samples,
	sum(if(perc_pkt_loss_up_at>=@thrh,1,0)) as pkt_loss_up_sat,
    sum(if(perc_pkt_loss_up_at<@thrh,1,0)) as pkt_loss_up_sbt,
    sum(delay_down_samples) as delay_down_samples,
	sum(if(perc_delay_down_at>=@thrh,1,0)) as delay_down_sat,
    sum(if(perc_delay_down_at<@thrh,1,0)) as delay_down_sbt,
    sum(delay_up_samples) as delay_up_samples,
	sum(if(perc_delay_up_at>=@thrh,1,0)) as delay_up_sat,
    sum(if(perc_delay_up_at<@thrh,1,0)) as delay_up_sbt,
    sum(pdv_down_samples) as pdv_down_samples,
	sum(if(perc_pdv_down_at>=@thrh,1,0)) as pdv_down_sat,
    sum(if(perc_pdv_down_at<@thrh,1,0)) as pdv_down_sbt,
    sum(pdv_up_samples) as pdv_up_samples,
	sum(if(perc_pdv_up_at>=@thrh,1,0)) as pdv_up_sat,
    sum(if(perc_pdv_up_at<@thrh,1,0)) as pdv_up_sbt
from
	(	
	select 
		date_format(a.data,'%Y-%U') as datex,
		a.uf,
		cn,
		a.site,
		city,
		cluster,
		avg(pkt_loss_down) as pkt_loss_down_avg,
		sum(if(pkt_loss_down>=@thrpl,1,0))/count(1) as perc_pkt_loss_down_at,
		count(pkt_loss_down) as pkt_loss_down_samples,
		sum(if(pkt_loss_down>=@thrpl,1,0)) as pkt_loss_down_at,
		sum(if(pkt_loss_down<@thrpl,1,0)) as pkt_loss_down_bt,
		avg(pkt_loss_up) as pkt_loss_up_avg,
		sum(if(pkt_loss_up>=@thrpl,1,0))/count(1) as perc_pkt_loss_up_at,
		count(pkt_loss_up) as pkt_loss_up_samples,
		sum(if(pkt_loss_up>=@thrpl,1,0)) as pkt_loss_up_at,
		sum(if(pkt_loss_up<@thrpl,1,0)) as pkt_loss_up_bt,
		avg(delay_down) as delay_down_avg,
		sum(if(delay_down>=@thrd,1,0))/count(1) as perc_delay_down_at,
		count(delay_down) as delay_down_samples,
		sum(if(delay_down>=@thrd,1,0)) as delay_down_at,
		sum(if(delay_down<@thrd,1,0)) as delay_down_bt,
		avg(delay_up) as delay_up_avg,
		sum(if(delay_up>=@thrd,1,0))/count(1) as perc_delay_up_at,
		count(delay_up) as delay_up_samples,
		sum(if(delay_up>=@thrd,1,0)) as delay_up_at,
		sum(if(delay_up<@thrd,1,0)) as delay_up_bt,
		avg(pdv_down) as pdv_down_avg,
		sum(if(pdv_down>=@thrpdv,1,0))/count(1) as perc_pdv_down_at,
		count(pdv_down) as pdv_down_samples,
		sum(if(pdv_down>=@thrpdv,1,0)) as pdv_down_at,
		sum(if(pdv_down<@thrpdv,1,0)) as pdv_down_bt,
		avg(pdv_up) as pdv_up_avg,
		sum(if(pdv_up>=@thrpdv,1,0))/count(1) as perc_pdv_up_at,
		count(pdv_up) as pdv_up_samples,
		sum(if(pdv_up>=@thrpdv,1,0)) as pdv_up_at,
		sum(if(pdv_up<@thrpdv,1,0)) as pdv_up_bt
	from 
		npm.tx_4g_accedian_hour as a
	left join
		npm.sites_ref as b
	on 
		a.uf=b.uf and mid(a.site,2,3)=b.site
	where
		data>=date_format(date_add(subdate(current_date(), interval @dago day), interval(1-dayofweek(subdate(current_date(), interval @dago day))) day), '%Y-%m-%d 00:00:00') and data<date_format(date_add(current_date(), interval(1-dayofweek(current_date())) day), '%Y-%m-%d 00:00:00')
		and hour(data) in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
		and pkt_loss_down is not null
	group by
		1,2,3,4,5,6
	) as c
group by
	1,2,3,4,5
;