use npm;
REPLACE INTO 
    npm.tx_3g_dash
SELECT 
    tbla.DATA AS DATETIME,
    tbla.UF AS UF,
    tblb.CN AS CN,
    UCASE(REPLACE(tblb.CLUSTER,"_"," ")) AS CLUSTER,
    CASE 
        WHEN LEFT(tbla.SITE,1)="W" THEN 'IUB'
        WHEN LEFT(tbla.SITE,4)="IUCS" THEN 'IUCS'
        WHEN LEFT(tbla.SITE,4)="IUCS" THEN 'IUPS'
        WHEN LEFT(tbla.SITE,3)="IUR" THEN 'IUR'
        WHEN LEFT(tbla.SITE,3)="RNC" THEN 'RNC'
    END AS INTERFACE,
    sum(if(AVGDELAY is not null,1,0)) as `AVGDELAY_SAMPLES`,
    sum(if(AVGDELAY is null,1,0)) as `AVGDELAY_CLASS_NULL`,
    sum(if(AVGDELAY is not null and AVGDELAY<=5,1,0)) as `AVGDELAY_CLASS_1`,
    sum(if(AVGDELAY is not null and AVGDELAY>5 and AVGDELAY<=10,1,0)) as `AVGDELAY_CLASS_2`,
    sum(if(AVGDELAY is not null and AVGDELAY>10 and AVGDELAY<=20,1,0)) as `AVGDELAY_CLASS_3`,
    sum(if(AVGDELAY is not null and AVGDELAY>20,1,0)) as `AVGDELAY_CLASS_4`,
    sum(if(MAXDELAY is not null,1,0)) as `MAXDELAY_SAMPLES`,
    sum(if(MAXDELAY is null,1,0)) as `MAXDELAY_CLASS_NULL`,
    sum(if(MAXDELAY is not null and MAXDELAY<=5,1,0)) as `MAXDELAY_CLASS_1`,
    sum(if(MAXDELAY is not null and MAXDELAY>5 and MAXDELAY<=10,1,0)) as `MAXDELAY_CLASS_2`,
    sum(if(MAXDELAY is not null and MAXDELAY>10 and MAXDELAY<=20,1,0)) as `MAXDELAY_CLASS_3`,
    sum(if(MAXDELAY is not null and MAXDELAY>20,1,0)) as `MAXDELAY_CLASS_4`,
    sum(if(AVGJITTER is not null,1,0)) as `AVGJITTER_SAMPLES`,
    sum(if(AVGJITTER is null,1,0)) as `AVGJITTER_CLASS_NULL`,
    sum(if(AVGJITTER is not null and AVGJITTER<=2,1,0)) as `AVGJITTER_CLASS_1`,
    sum(if(AVGJITTER is not null and AVGJITTER>2 and AVGJITTER<=4,1,0)) as `AVGJITTER_CLASS_2`,
    sum(if(AVGJITTER is not null and AVGJITTER>4 and AVGJITTER<=8,1,0)) as `AVGJITTER_CLASS_3`,
    sum(if(AVGJITTER is not null and AVGJITTER>8,1,0)) as `AVGJITTER_CLASS_4`,
    sum(if(MAXJITTER is not null,1,0)) as `MAXJITTER_SAMPLES`,
    sum(if(MAXJITTER is null,1,0)) as `MAXJITTER_CLASS_NULL`,
    sum(if(MAXJITTER is not null and MAXJITTER<=2,1,0)) as `MAXJITTER_CLASS_1`,
    sum(if(MAXJITTER is not null and MAXJITTER>2 and MAXJITTER<=4,1,0)) as `MAXJITTER_CLASS_2`,
    sum(if(MAXJITTER is not null and MAXJITTER>4 and MAXJITTER<=8,1,0)) as `MAXJITTER_CLASS_3`,
    sum(if(MAXJITTER is not null and MAXJITTER>8,1,0)) as `MAXJITTER_CLASS_4`,
    sum(if(AVGLOST is not null,1,0)) as `AVGLOST_SAMPLES`,
    sum(if(AVGLOST is null,1,0)) as `AVGLOST_CLASS_NULL`,
    sum(if(AVGLOST is not null and AVGLOST=0,1,0)) as `AVGLOST_CLASS_1`,
    sum(if(AVGLOST is not null and AVGLOST>0 and AVGLOST<=0.1,1,0)) as `AVGLOST_CLASS_2`,
    sum(if(AVGLOST is not null and AVGLOST>0.1 and AVGLOST<=0.5,1,0)) as `AVGLOST_CLASS_3`,
    sum(if(AVGLOST is not null and AVGLOST>0.5,1,0)) as `AVGLOST_CLASS_4`,
    sum(if(MAXLOST is not null,1,0)) as `MAXLOST_SAMPLES`,
    sum(if(MAXLOST is null,1,0)) as `MAXLOST_CLASS_NULL`,
    sum(if(MAXLOST is not null and MAXLOST=0,1,0)) as `MAXLOST_CLASS_1`,
    sum(if(MAXLOST is not null and MAXLOST>0 and MAXLOST<=0.1,1,0)) as `MAXLOST_CLASS_2`,
    sum(if(MAXLOST is not null and MAXLOST>0.1 and MAXLOST<=0.5,1,0)) as `MAXLOST_CLASS_3`,
    sum(if(MAXLOST is not null and MAXLOST>0.5,1,0)) as `MAXLOST_CLASS_4`
FROM 
    npm.tx_3g_ipping_hour AS tbla
LEFT JOIN
    npm.sites_ref as tblb
ON 
    tbla.uf=tblb.uf and SUBSTRING(tbla.site,2,3)=tblb.site
WHERE
    tbla.DATA > curdate() - INTERVAL 2 DAY
    and left(tbla.site,1)="W"
    and tblb.CN is not null
    and tblb.CLUSTER is not null
GROUP BY
    1, 2, 3, 4, 5
;