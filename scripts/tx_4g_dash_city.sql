use npm;
REPLACE INTO 
	npm.tx_4g_dash_city
SELECT 
	tbla.DATA AS DATETIME,
	if(tbla.UF is null, 'UNKNOWN', tbla.UF) AS UF,
	if(tblb.CN is null, 'UNKNOWN', tblb.CN) AS CN,
    if(tblb.CITY is null, 'UNKNOWN', tblb.CITY) AS CITY,
	if(tblb.CLUSTER is null, 'UNKNOWN', UCASE(REPLACE(tblb.CLUSTER,"_"," "))) AS CLUSTER,
	avg(DELAY_DOWN) as DELAY_DOWN_AVG,
	avg(DELAY_UP) as DELAY_UP_AVG,
	avg(PDV_DOWN) as PDV_DOWN_AVG,
	avg(PDV_UP) as PDV_UP_AVG,
	avg(PKT_LOSS_DOWN) as PKT_LOSS_DOWN_AVG,
	avg(PKT_LOSS_UP) as PKT_LOSS_UP_AVG,
	
	sum(if(DELAY_DOWN is not null,1,0)) as `DELAY_DOWN_SAMPLES`,
	sum(if(DELAY_DOWN is null,1,0)) as `DELAY_DOWN_CLASS_NULL`,
	sum(if(DELAY_DOWN is not null and DELAY_DOWN<=5,1,0)) as `DELAY_DOWN_CLASS_1`,
	sum(if(DELAY_DOWN is not null and DELAY_DOWN>5 and DELAY_DOWN<=10,1,0)) as `DELAY_DOWN_CLASS_2`,
	sum(if(DELAY_DOWN is not null and DELAY_DOWN>10 and DELAY_DOWN<=20,1,0)) as `DELAY_DOWN_CLASS_3`,
	sum(if(DELAY_DOWN is not null and DELAY_DOWN>20,1,0)) as `DELAY_DOWN_CLASS_4`,
	
	sum(if(DELAY_UP is not null,1,0)) as `DELAY_UP_SAMPLES`,
	sum(if(DELAY_UP is null,1,0)) as `DELAY_UP_CLASS_NULL`,
	sum(if(DELAY_UP is not null and DELAY_UP<=5,1,0)) as `DELAY_UP_CLASS_1`,
	sum(if(DELAY_UP is not null and DELAY_UP>5 and DELAY_UP<=10,1,0)) as `DELAY_UP_CLASS_2`,
	sum(if(DELAY_UP is not null and DELAY_UP>10 and DELAY_UP<=20,1,0)) as `DELAY_UP_CLASS_3`,
	sum(if(DELAY_UP is not null and DELAY_UP>20,1,0)) as `DELAY_UP_CLASS_4`,
	
	sum(if(PDV_DOWN is not null,1,0)) as `PDV_DOWN_SAMPLES`,
	sum(if(PDV_DOWN is null,1,0)) as `PDV_DOWN_CLASS_NULL`,
	sum(if(PDV_DOWN is not null and PDV_DOWN<=2,1,0)) as `PDV_DOWN_CLASS_1`,
	sum(if(PDV_DOWN is not null and PDV_DOWN>2 and PDV_DOWN<=4,1,0)) as `PDV_DOWN_CLASS_2`,
	sum(if(PDV_DOWN is not null and PDV_DOWN>4 and PDV_DOWN<=8,1,0)) as `PDV_DOWN_CLASS_3`,
	sum(if(PDV_DOWN is not null and PDV_DOWN>8,1,0)) as `PDV_DOWN_CLASS_4`,
	
	sum(if(PDV_UP is not null,1,0)) as `PDV_UP_SAMPLES`,
	sum(if(PDV_UP is null,1,0)) as `PDV_UP_CLASS_NULL`,
	sum(if(PDV_UP is not null and PDV_UP<=2,1,0)) as `PDV_UP_CLASS_1`,
	sum(if(PDV_UP is not null and PDV_UP>2 and PDV_UP<=4,1,0)) as `PDV_UP_CLASS_2`,
	sum(if(PDV_UP is not null and PDV_UP>4 and PDV_UP<=8,1,0)) as `PDV_UP_CLASS_3`,
	sum(if(PDV_UP is not null and PDV_UP>8,1,0)) as `PDV_UP_CLASS_4`,
	
	sum(if(PKT_LOSS_DOWN is not null,1,0)) as `PKT_LOSS_DOWN_SAMPLES`,
	sum(if(PKT_LOSS_DOWN is null,1,0)) as `PKT_LOSS_DOWN_CLASS_NULL`,
	sum(if(PKT_LOSS_DOWN is not null and PKT_LOSS_DOWN=0,1,0)) as `PKT_LOSS_DOWN_CLASS_1`,
	sum(if(PKT_LOSS_DOWN is not null and PKT_LOSS_DOWN>0 and PKT_LOSS_DOWN<=0.1,1,0)) as `PKT_LOSS_DOWN_CLASS_2`,
	sum(if(PKT_LOSS_DOWN is not null and PKT_LOSS_DOWN>0.1 and PKT_LOSS_DOWN<=0.5,1,0)) as `PKT_LOSS_DOWN_CLASS_3`,
	sum(if(PKT_LOSS_DOWN is not null and PKT_LOSS_DOWN>0.5,1,0)) as `PKT_LOSS_DOWN_CLASS_4`,
	
	sum(if(PKT_LOSS_UP is not null,1,0)) as `PKT_LOSS_UP_SAMPLES`,
	sum(if(PKT_LOSS_UP is null,1,0)) as `PKT_LOSS_UP_CLASS_NULL`,
	sum(if(PKT_LOSS_UP is not null and PKT_LOSS_UP=0,1,0)) as `PKT_LOSS_UP_CLASS_1`,
	sum(if(PKT_LOSS_UP is not null and PKT_LOSS_UP>0 and PKT_LOSS_UP<=0.1,1,0)) as `PKT_LOSS_UP_CLASS_2`,
	sum(if(PKT_LOSS_UP is not null and PKT_LOSS_UP>0.1 and PKT_LOSS_UP<=0.5,1,0)) as `PKT_LOSS_UP_CLASS_3`,
	sum(if(PKT_LOSS_UP is not null and PKT_LOSS_UP>0.5,1,0)) as `PKT_LOSS_UP_CLASS_4`
FROM 
	npm.tx_4g_accedian_hour AS tbla
LEFT JOIN
	sites_ref as tblb
ON 
	tbla.uf=tblb.uf and SUBSTRING(tbla.site,2,3)=tblb.site
WHERE
	tbla.DATA >= curdate() - INTERVAL 4 DAY
GROUP BY
	1, 2, 3, 4 
;