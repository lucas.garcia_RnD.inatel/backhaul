SELECT
    T1.UF,
    T1.CLUSTER,	
    
    CONCAT(
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_DOWN_CLASS_4-MIN(T2.PKT_LOSS_DOWN_CLASS_4))/(MAX(T2.PKT_LOSS_DOWN_CLASS_4)-MIN(T2.PKT_LOSS_DOWN_CLASS_4))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_DOWN_CLASS_3-MIN(T2.PKT_LOSS_DOWN_CLASS_3))/(MAX(T2.PKT_LOSS_DOWN_CLASS_3)-MIN(T2.PKT_LOSS_DOWN_CLASS_3))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_DOWN_CLASS_2-MIN(T2.PKT_LOSS_DOWN_CLASS_2))/(MAX(T2.PKT_LOSS_DOWN_CLASS_2)-MIN(T2.PKT_LOSS_DOWN_CLASS_2))*9999,0),0),4,0)
    ,    
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_DOWN_CLASS_1-MIN(T2.PKT_LOSS_DOWN_CLASS_1))/(MAX(T2.PKT_LOSS_DOWN_CLASS_1)-MIN(T2.PKT_LOSS_DOWN_CLASS_1))*9999,0),0),4,0)
    )AS PKT_LOSS_DOWN_RANK,
	
	CONCAT(
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_UP_CLASS_4-MIN(T2.PKT_LOSS_UP_CLASS_4))/(MAX(T2.PKT_LOSS_UP_CLASS_4)-MIN(T2.PKT_LOSS_UP_CLASS_4))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_UP_CLASS_3-MIN(T2.PKT_LOSS_UP_CLASS_3))/(MAX(T2.PKT_LOSS_UP_CLASS_3)-MIN(T2.PKT_LOSS_UP_CLASS_3))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_UP_CLASS_2-MIN(T2.PKT_LOSS_UP_CLASS_2))/(MAX(T2.PKT_LOSS_UP_CLASS_2)-MIN(T2.PKT_LOSS_UP_CLASS_2))*9999,0),0),4,0)
    ,    
    LPAD(ROUND(IFNULL((T1.PKT_LOSS_UP_CLASS_1-MIN(T2.PKT_LOSS_UP_CLASS_1))/(MAX(T2.PKT_LOSS_UP_CLASS_1)-MIN(T2.PKT_LOSS_UP_CLASS_1))*9999,0),0),4,0)
    )AS PKT_LOSS_UP_RANK,
	
	CONCAT(
    LPAD(ROUND(IFNULL((T1.DELAY_DOWN_CLASS_4-MIN(T2.DELAY_DOWN_CLASS_4))/(MAX(T2.DELAY_DOWN_CLASS_4)-MIN(T2.DELAY_DOWN_CLASS_4))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.DELAY_DOWN_CLASS_3-MIN(T2.DELAY_DOWN_CLASS_3))/(MAX(T2.DELAY_DOWN_CLASS_3)-MIN(T2.DELAY_DOWN_CLASS_3))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.DELAY_DOWN_CLASS_2-MIN(T2.DELAY_DOWN_CLASS_2))/(MAX(T2.DELAY_DOWN_CLASS_2)-MIN(T2.DELAY_DOWN_CLASS_2))*9999,0),0),4,0)
    ,    
    LPAD(ROUND(IFNULL((T1.DELAY_DOWN_CLASS_1-MIN(T2.DELAY_DOWN_CLASS_1))/(MAX(T2.DELAY_DOWN_CLASS_1)-MIN(T2.DELAY_DOWN_CLASS_1))*9999,0),0),4,0)
    )AS DELAY_DOWN_RANK,
	
	CONCAT(
    LPAD(ROUND(IFNULL((T1.DELAY_UP_CLASS_4-MIN(T2.DELAY_UP_CLASS_4))/(MAX(T2.DELAY_UP_CLASS_4)-MIN(T2.DELAY_UP_CLASS_4))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.DELAY_UP_CLASS_3-MIN(T2.DELAY_UP_CLASS_3))/(MAX(T2.DELAY_UP_CLASS_3)-MIN(T2.DELAY_UP_CLASS_3))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.DELAY_UP_CLASS_2-MIN(T2.DELAY_UP_CLASS_2))/(MAX(T2.DELAY_UP_CLASS_2)-MIN(T2.DELAY_UP_CLASS_2))*9999,0),0),4,0)
    ,    
    LPAD(ROUND(IFNULL((T1.DELAY_UP_CLASS_1-MIN(T2.DELAY_UP_CLASS_1))/(MAX(T2.DELAY_UP_CLASS_1)-MIN(T2.DELAY_UP_CLASS_1))*9999,0),0),4,0)
    )AS DELAY_UP_RANK,
	
	CONCAT(
    LPAD(ROUND(IFNULL((T1.PDV_DOWN_CLASS_4-MIN(T2.PDV_DOWN_CLASS_4))/(MAX(T2.PDV_DOWN_CLASS_4)-MIN(T2.PDV_DOWN_CLASS_4))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PDV_DOWN_CLASS_3-MIN(T2.PDV_DOWN_CLASS_3))/(MAX(T2.PDV_DOWN_CLASS_3)-MIN(T2.PDV_DOWN_CLASS_3))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PDV_DOWN_CLASS_2-MIN(T2.PDV_DOWN_CLASS_2))/(MAX(T2.PDV_DOWN_CLASS_2)-MIN(T2.PDV_DOWN_CLASS_2))*9999,0),0),4,0)
    ,    
    LPAD(ROUND(IFNULL((T1.PDV_DOWN_CLASS_1-MIN(T2.PDV_DOWN_CLASS_1))/(MAX(T2.PDV_DOWN_CLASS_1)-MIN(T2.PDV_DOWN_CLASS_1))*9999,0),0),4,0)
    )AS PDV_DOWN_RANK,
	
	CONCAT(
    LPAD(ROUND(IFNULL((T1.PDV_UP_CLASS_4-MIN(T2.PDV_UP_CLASS_4))/(MAX(T2.PDV_UP_CLASS_4)-MIN(T2.PDV_UP_CLASS_4))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PDV_UP_CLASS_3-MIN(T2.PDV_UP_CLASS_3))/(MAX(T2.PDV_UP_CLASS_3)-MIN(T2.PDV_UP_CLASS_3))*9999,0),0),4,0)
    ,
    LPAD(ROUND(IFNULL((T1.PDV_UP_CLASS_2-MIN(T2.PDV_UP_CLASS_2))/(MAX(T2.PDV_UP_CLASS_2)-MIN(T2.PDV_UP_CLASS_2))*9999,0),0),4,0)
    ,    
    LPAD(ROUND(IFNULL((T1.PDV_UP_CLASS_1-MIN(T2.PDV_UP_CLASS_1))/(MAX(T2.PDV_UP_CLASS_1)-MIN(T2.PDV_UP_CLASS_1))*9999,0),0),4,0)
    )AS PDV_UP_RANK
	
FROM
    (
    select 
        UF,
        CLUSTER,
        sum(PKT_LOSS_DOWN_CLASS_1)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_1,
        sum(PKT_LOSS_DOWN_CLASS_2)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_2,
        sum(PKT_LOSS_DOWN_CLASS_3)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_3,
        sum(PKT_LOSS_DOWN_CLASS_4)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_4,
        sum(PKT_LOSS_UP_CLASS_1)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_1,
        sum(PKT_LOSS_UP_CLASS_2)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_2,
        sum(PKT_LOSS_UP_CLASS_3)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_3,
        sum(PKT_LOSS_UP_CLASS_4)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_4,
            sum(DELAY_DOWN_CLASS_1)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_1,
        sum(DELAY_DOWN_CLASS_2)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_2,
        sum(DELAY_DOWN_CLASS_3)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_3,
        sum(DELAY_DOWN_CLASS_4)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_4,
        sum(DELAY_UP_CLASS_1)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_1,
        sum(DELAY_UP_CLASS_2)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_2,
        sum(DELAY_UP_CLASS_3)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_3,
        sum(DELAY_UP_CLASS_4)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_4,
        sum(PDV_DOWN_CLASS_1)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_1,
        sum(PDV_DOWN_CLASS_2)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_2,
        sum(PDV_DOWN_CLASS_3)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_3,
        sum(PDV_DOWN_CLASS_4)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_4,
        sum(PDV_UP_CLASS_1)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_1,
        sum(PDV_UP_CLASS_2)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_2,
        sum(PDV_UP_CLASS_3)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_3,
        sum(PDV_UP_CLASS_4)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_4
    from 
            npm.tx_4g_dash
    where
            DATETIME > curdate() - interval 10 day
    group by 
            UF,
            CLUSTER
    ) AS T1
JOIN
    (
    select 
        UF,
        CLUSTER,
        sum(PKT_LOSS_DOWN_CLASS_1)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_1,
        sum(PKT_LOSS_DOWN_CLASS_2)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_2,
        sum(PKT_LOSS_DOWN_CLASS_3)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_3,
        sum(PKT_LOSS_DOWN_CLASS_4)/sum(PKT_LOSS_DOWN_CLASS_1+PKT_LOSS_DOWN_CLASS_2+PKT_LOSS_DOWN_CLASS_3+PKT_LOSS_DOWN_CLASS_4) AS PKT_LOSS_DOWN_CLASS_4,
        sum(PKT_LOSS_UP_CLASS_1)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_1,
        sum(PKT_LOSS_UP_CLASS_2)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_2,
        sum(PKT_LOSS_UP_CLASS_3)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_3,
        sum(PKT_LOSS_UP_CLASS_4)/sum(PKT_LOSS_UP_CLASS_1+PKT_LOSS_UP_CLASS_2+PKT_LOSS_UP_CLASS_3+PKT_LOSS_UP_CLASS_4) as PKT_LOSS_UP_CLASS_4,
            sum(DELAY_DOWN_CLASS_1)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_1,
        sum(DELAY_DOWN_CLASS_2)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_2,
        sum(DELAY_DOWN_CLASS_3)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_3,
        sum(DELAY_DOWN_CLASS_4)/sum(DELAY_DOWN_CLASS_1+DELAY_DOWN_CLASS_2+DELAY_DOWN_CLASS_3+DELAY_DOWN_CLASS_4) as DELAY_DOWN_CLASS_4,
        sum(DELAY_UP_CLASS_1)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_1,
        sum(DELAY_UP_CLASS_2)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_2,
        sum(DELAY_UP_CLASS_3)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_3,
        sum(DELAY_UP_CLASS_4)/sum(DELAY_UP_CLASS_1+DELAY_UP_CLASS_2+DELAY_UP_CLASS_3+DELAY_UP_CLASS_4) as DELAY_UP_CLASS_4,
        sum(PDV_DOWN_CLASS_1)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_1,
        sum(PDV_DOWN_CLASS_2)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_2,
        sum(PDV_DOWN_CLASS_3)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_3,
        sum(PDV_DOWN_CLASS_4)/sum(PDV_DOWN_CLASS_1+PDV_DOWN_CLASS_2+PDV_DOWN_CLASS_3+PDV_DOWN_CLASS_4) as PDV_DOWN_CLASS_4,
        sum(PDV_UP_CLASS_1)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_1,
        sum(PDV_UP_CLASS_2)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_2,
        sum(PDV_UP_CLASS_3)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_3,
        sum(PDV_UP_CLASS_4)/sum(PDV_UP_CLASS_1+PDV_UP_CLASS_2+PDV_UP_CLASS_3+PDV_UP_CLASS_4) as PDV_UP_CLASS_4
    from 
            npm.tx_4g_dash
    where
            DATETIME > curdate() - interval 10 day
    group by 
            UF,
            CLUSTER
    ) AS T2
ON 
    T1.UF=T2.UF
GROUP BY
    T1.UF,
    T1.CLUSTER