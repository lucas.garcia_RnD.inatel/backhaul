use npm;

set @dago=28;

--Pkt Loss Down

set @kpi='pkt_loss_down';
set @thrh=0.1;

replace into
	npm.tx_4g_offenders_list
select 
	date_format(data,'%Y-%U') as datex,
	a.uf,
	if(cn is null,'NA',cn) as cn,
	if(city is null,'NA',city) as city,
	if(cluster is null,'NA',cluster) as cluster,
	a.site,
	@kpi as kpi_name,
	@thrh as target,
	round(avg(pkt_loss_down),8) as kpi_avg,
	count(1) as kpi_samples,
	sum(if(pkt_loss_down>=@thrh,1,0)) as samples_above_target,
	sum(if(pkt_loss_down<@thrh,1,0)) as samples_below_target,
	round(sum(if(pkt_loss_down>=@thrh,1,0))/count(1),8) as perc_samples_above_target,
	round(sum(if(pkt_loss_down<@thrh,1,0))/count(1),8) as perc_samples_below_target
from 
	npm.tx_4g_accedian_hour as a
left join
	npm.sites_ref as b
on 
	a.uf=b.uf and mid(a.site,2,3)=b.site
where
	data>=date_format(date_add(subdate(current_date(), interval @dago day), interval(1-dayofweek(subdate(current_date(), interval @dago day))) day), '%Y-%m-%d 00:00:00') and data<date_format(date_add(current_date(), interval(1-dayofweek(current_date())) day), '%Y-%m-%d 00:00:00')
	and hour(data) in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
	and pkt_loss_down is not null
group by
	1,2,3,4,5,6,7,8
;

--Pkt Loss Up

set @kpi='pkt_loss_up';
set @thrh=0.1;

replace into
	npm.tx_4g_offenders_list
select 
	date_format(data,'%Y-%U') as datex,
	a.uf,
	if(cn is null,'NA',cn) as cn,
	if(city is null,'NA',city) as city,
	if(cluster is null,'NA',cluster) as cluster,
	a.site,
	@kpi as kpi_name,
	@thrh as target,
	round(avg(pkt_loss_up),8) as kpi_avg,
	count(1) as kpi_samples,
	sum(if(pkt_loss_up>=@thrh,1,0)) as samples_above_target,
	sum(if(@pkt_loss_up<@thrh,1,0)) as samples_below_target,
	round(sum(if(pkt_loss_up>=@thrh,1,0))/count(1),8) as perc_samples_above_target,
	round(sum(if(pkt_loss_up<@thrh,1,0))/count(1),8) as perc_samples_below_target
from 
	npm.tx_4g_accedian_hour as a
left join
	npm.sites_ref as b
on 
	a.uf=b.uf and mid(a.site,2,3)=b.site
where
	data>=date_format(date_add(subdate(current_date(), interval @dago day), interval(1-dayofweek(subdate(current_date(), interval @dago day))) day), '%Y-%m-%d 00:00:00') and data<date_format(date_add(current_date(), interval(1-dayofweek(current_date())) day), '%Y-%m-%d 00:00:00')
	and hour(data) in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
	and pkt_loss_up is not null
group by
	1,2,3,4,5,6,7,8
;

--Delay Down

set @kpi='delay_down';
set @thrh=20;

replace into
	npm.tx_4g_offenders_list
select 
	date_format(data,'%Y-%U') as datex,
	a.uf,
	if(cn is null,'NA',cn) as cn,
	if(city is null,'NA',city) as city,
	if(cluster is null,'NA',cluster) as cluster,
	a.site,
	@kpi as kpi_name,
	@thrh as target,
	round(avg(delay_down),8) as kpi_avg,
	count(1) as kpi_samples,
	sum(if(delay_down>=@thrh,1,0)) as samples_above_target,
	sum(if(delay_down<@thrh,1,0)) as samples_below_target,
	round(sum(if(delay_down>=@thrh,1,0))/count(1),8) as perc_samples_above_target,
	round(sum(if(delay_down<@thrh,1,0))/count(1),8) as perc_samples_below_target
from 
	npm.tx_4g_accedian_hour as a
left join
	npm.sites_ref as b
on 
	a.uf=b.uf and mid(a.site,2,3)=b.site
where
	data>=date_format(date_add(subdate(current_date(), interval @dago day), interval(1-dayofweek(subdate(current_date(), interval @dago day))) day), '%Y-%m-%d 00:00:00') and data<date_format(date_add(current_date(), interval(1-dayofweek(current_date())) day), '%Y-%m-%d 00:00:00')
	and hour(data) in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
	and delay_down is not null
group by
	1,2,3,4,5,6,7,8
;

--Delay Up

set @kpi='delay_up';
set @thrh=20;

replace into
	npm.tx_4g_offenders_list
select 
	date_format(data,'%Y-%U') as datex,
	a.uf,
	if(cn is null,'NA',cn) as cn,
	if(city is null,'NA',city) as city,
	if(cluster is null,'NA',cluster) as cluster,
	a.site,
	@kpi as kpi_name,
	@thrh as target,
	round(avg(delay_up),8) as kpi_avg,
	count(1) as kpi_samples,
	sum(if(delay_up>=@thrh,1,0)) as samples_above_target,
	sum(if(delay_up<@thrh,1,0)) as samples_below_target,
	round(sum(if(delay_up>=@thrh,1,0))/count(1),8) as perc_samples_above_target,
	round(sum(if(delay_up<@thrh,1,0))/count(1),8) as perc_samples_below_target
from 
	npm.tx_4g_accedian_hour as a
left join
	npm.sites_ref as b
on 
	a.uf=b.uf and mid(a.site,2,3)=b.site
where
	data>=date_format(date_add(subdate(current_date(), interval @dago day), interval(1-dayofweek(subdate(current_date(), interval @dago day))) day), '%Y-%m-%d 00:00:00') and data<date_format(date_add(current_date(), interval(1-dayofweek(current_date())) day), '%Y-%m-%d 00:00:00')
	and hour(data) in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
	and @kpi is not null
group by
	1,2,3,4,5,6,7,8
;

--PDV Down

set @kpi='pdv_down';
set @thrh=8;

replace into
	npm.tx_4g_offenders_list
select 
	date_format(data,'%Y-%U') as datex,
	a.uf,
	if(cn is null,'NA',cn) as cn,
	if(city is null,'NA',city) as city,
	if(cluster is null,'NA',cluster) as cluster,
	a.site,
	@kpi as kpi_name,
	@thrh as target,
	round(avg(pdv_down),8) as kpi_avg,
	count(1) as kpi_samples,
	sum(if(pdv_down>=@thrh,1,0)) as samples_above_target,
	sum(if(pdv_down<@thrh,1,0)) as samples_below_target,
	round(sum(if(pdv_down>=@thrh,1,0))/count(1),8) as perc_samples_above_target,
	round(sum(if(pdv_down<@thrh,1,0))/count(1),8) as perc_samples_below_target
from 
	npm.tx_4g_accedian_hour as a
left join
	npm.sites_ref as b
on 
	a.uf=b.uf and mid(a.site,2,3)=b.site
where
	data>=date_format(date_add(subdate(current_date(), interval @dago day), interval(1-dayofweek(subdate(current_date(), interval @dago day))) day), '%Y-%m-%d 00:00:00') and data<date_format(date_add(current_date(), interval(1-dayofweek(current_date())) day), '%Y-%m-%d 00:00:00')
	and hour(data) in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
	and pdv_down is not null
group by
	1,2,3,4,5,6,7,8
;

--PDV Up

set @kpi='pdv_up';
set @thrh=8;

replace into
	npm.tx_4g_offenders_list
select 
	date_format(data,'%Y-%U') as datex,
	a.uf,
	if(cn is null,'NA',cn) as cn,
	if(city is null,'NA',city) as city,
	if(cluster is null,'NA',cluster) as cluster,
	a.site,
	@kpi as kpi_name,
	@thrh as target,
	round(avg(pdv_up),8) as kpi_avg,
	count(1) as kpi_samples,
	sum(if(pdv_up>=@thrh,1,0)) as samples_above_target,
	sum(if(pdv_up<@thrh,1,0)) as samples_below_target,
	round(sum(if(pdv_up>=@thrh,1,0))/count(1),8) as perc_samples_above_target,
	round(sum(if(pdv_up<@thrh,1,0))/count(1),8) as perc_samples_below_target
from 
	npm.tx_4g_accedian_hour as a
left join
	npm.sites_ref as b
on 
	a.uf=b.uf and mid(a.site,2,3)=b.site
where
	data>=date_format(date_add(subdate(current_date(), interval @dago day), interval(1-dayofweek(subdate(current_date(), interval @dago day))) day), '%Y-%m-%d 00:00:00') and data<date_format(date_add(current_date(), interval(1-dayofweek(current_date())) day), '%Y-%m-%d 00:00:00')
	and hour(data) in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
	and pdv_up is not null
group by
	1,2,3,4,5,6,7,8
;