#######MODULOS BASICOS######
import os
import sys
import argparse
########MODULOS AUTOMAÇÂO WEB#########
from selenium import webdriver
from selenium.common.exceptions import  NoSuchElementException
#######MODULOS PARA INTERAGIR COM O BACOS###############
from Utils import *

from datetime import datetime

pwd = os.getcwd()
start_time = time.time()

basepath = os.path.dirname(__file__)
downloads_path = os.path.abspath(os.path.join(basepath, "downloads"))
bkp_path = os.path.abspath(os.path.join(downloads_path, 'BKP'))

parser = argparse.ArgumentParser(description='Esse script faz a coleta automática de indicadores no Portal de Desempenho Backhaul.')
parser.add_argument("--SelectTime", action="extend", nargs="+", type=str, help="<relative ou specific_to_date ou specific_to_time> <DeltaH ou Y-M-D ou Y-M-D_H:M:S> <Y-M-D ou Y-M-D_H:M:S>")
parser.add_argument("--tryagain", default=False, action='store_true', help="tenta denovo se der erro")
parser.add_argument("--loop", default=False, action='store_true', help="faz o script rodar por tempo indeterminado")
#parser.add_argument("--bot", default=False, action='store_true', help="ativa comunicação com o EstagBot")
#parser.add_argument('--sem', default=False, action='store_true', help="apenas coleta semanal")
#parser.add_argument('--hour', default=False, action='store_true', help="apenas coleta horária")
parser.add_argument('--nodownload', default=True, action='store_false', help="o download ja ta lá")
parser.add_argument('--justdownload', default=False, action='store_true', help="apenas processo de download")
parser.add_argument('--noheadless', default=True, action='store_false', help="mostra todo o processo de automação do navegador")
#parser.add_argument('--insert', default=False, action='store_true', help="Usar INSERT ao invés de REPLACE")
parser.add_argument('--runSQL', default=False, action='store_true', help="Rodar query SQL ao final da coleta")
#parser.add_argument('--ignore', default=False, action='store_true', help="Usar INSERT IGNORE ao invés de REPLACE")
args = parser.parse_args()
#carrega onfigurações
config_BCKH = load_yaml()
#pasta onde o seu navegador faz os downloads dos csvs
path_to_downloads = pwd+config_BCKH['PATHS']['path_to_downloads']
#Login Huawei PM
username = config_BCKH['Backhaul']['username']
senha = config_BCKH['Backhaul']['senha']
NE = config_BCKH['NE']
#Roda em background
headless = args.noheadless
#Roda a porra toda em background
runSQL = args.runSQL
# #processa argumentos do --SelectTime
# select_time = prepara_tempo(args)
#
# start_date, end_date = seleciona_tempo(select_time)
#
# #data = (datetime.datetime(2020, 10, 13), datetime.datetime(2020, 10, 14))
# data = (start_date, end_date)


data_teste =[
    '2022-08-29',
    '2022-08-28',
    '2022-08-27',
    '2022-08-26',
    '2022-08-25',
    '2022-08-24',
    '2022-08-23',
    '2022-08-22',
    '2022-08-21',
    '2022-08-20',
    '2022-08-19',
    '2022-08-18',
    '2022-08-17',
    '2022-08-16',
    '2022-08-15',
    '2022-08-14',
    '2022-08-13',
    '2022-08-12',
    '2022-08-11',
    '2022-08-10',
    '2022-08-09',
    '2022-08-08',
    '2022-08-07',
    '2022-08-06',
    '2022-08-05',
    '2022-08-04',
    '2022-08-03',
    '2022-08-02',
    '2022-08-01'
]

try:

    ########FODA-SE SSL/TSL#################
    options = webdriver.ChromeOptions()
    options.add_argument('ignore-certificate-errors')
    options.add_argument('--start-maximized') #

    #define se vai rodar em modo headless
    if headless:
        options.add_argument("--headless") # Runs Chrome in headless mode.
        options.add_argument('--no-sandbox') # Bypass OS security mode
        options.add_argument('--disable-gpu')  # applicable to windows os only
        options.add_argument('--lang=en_US')

    ###########INICIA WEBDRIVER#############
    try:
        driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub',options=options)
    except Exception:
        print("Faltou iniciar o Selenium Server!")
        exit()

    ##########HABILITA WEBDRIVER A FAZER DOWNLOAD##########
    driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': path_to_downloads}}
    command_result = driver.execute("send_command", params)
    driver.get("https://ginfo.vivo.com.br/autenticacao/client/logon.htm?s=UE9SVEFMX0JBQ0tIQVVM&p=aHR0cHM6Ly9naW5mby52aXZvLmNvbS5ici9wb3J0YWxfYmFja2hhdWwvIy9xdWF0cm9H")

    ##########FazLogin##########
    try:
        element = driver.find_element(By.ID,"txtUsuario")
        element.clear()
        element.send_keys(username)
        element = driver.find_element(By.ID,"txtSenha")
        element.clear()
        element.send_keys(senha,Keys.RETURN)
        time.sleep(4)
        number = 0

    except NoSuchElementException:
        print("O site do Backhaul não carregou, checa se a VPN está conectada!")

    ##########FAZ DOWNLOAD / UPLOAD das coletas configuradas##########
    try:
        #print(data)
        #tempo_inicio_coleta, tempo_fim_coleta = (data[0], data[1])
        ###area de teste#####

        for datinha in data_teste:
            tempo_inicio_coleta = datetime.strptime(datinha,'%Y-%m-%d')
            tempo_fim_coleta = tempo_inicio_coleta
            print(f'Processando dia {tempo_inicio_coleta}')

            for coleta in config_BCKH['Coletas']:

                if args.nodownload:
                    print('Coletando '+coleta)
                    #Seleciona seletores <select>
                    for seletor in config_BCKH['Coletas'][coleta]['seletores']:
                        print(coleta,seletor,config_BCKH['Coletas'][coleta]['seletores'][seletor])
                        seleciona_existente(seletor,config_BCKH['Coletas'][coleta]['seletores'][seletor],driver)

                    escreve_datas(tempo_inicio_coleta.strftime("%d/%m/%Y"), tempo_fim_coleta.strftime("%d/%m/%Y"),driver)

                    n_files = len(os.listdir(path_to_downloads))

                    print('limpando pasta de downloads...')
                    #clean BKP
                    for fn in os.listdir(bkp_path):
                        os.remove(os.path.join(bkp_path,fn))
                    #execute BKP
                    for fn in os.listdir(path_to_downloads):
                        if '.csv' in fn:
                            os.rename(os.path.join(path_to_downloads,fn),os.path.join(bkp_path,fn))
                    #Clean Download
                    for fn in os.listdir(path_to_downloads):
                        if not '.htm.' in fn:
                            os.remove(os.path.join(path_to_downloads,fn))

                    ####SALVA####
                    espera_clica('//*/button[contains(text(),"Excel")]',driver)

                    espera_download_comecar(path_to_downloads)
                    espera_download(path_to_downloads)

                print('subindo arquivos')
                try:
                    arquivo = pandas.read_csv(path_to_downloads+'resultado'+'.xls',sep=';',decimal=',')
                except Exception as e:
                    print(f'Erro no arquivo baixado: {e}')
                    continue

                insertCSVIntoTable(arquivo,config_BCKH['Coletas'][coleta]['tabela_bacos'],path_to_downloads,NE,args)

        print('Tempo de execução: '+str(time.time() - start_time))
        driver.quit()

    except KeyboardInterrupt:
        print('Ctrl-C')
        driver.quit()

    #executando SQL
    if runSQL:
        start_time = time.time()
        script_list = config_BCKH['SCRIPTS']
        #running
        runSQLScripts(script_list)
        print('Tempo de execução SQL: '+str(time.time() - start_time))
    else:
        print(f'runSQL bypass!')

except not KeyboardInterrupt:
        print('Houve algum problema em algum procedimento não mapeado')