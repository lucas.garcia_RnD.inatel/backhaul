@echo off

rem Arguments Options
rem python "%~dp0\BackhaulExtractor.py" --SelectTime relative 96
rem python "%~dp0\BackhaulExtractor.py" --noheadless --SelectTime specific_to_date 2021-08-28 2021-08-29
rem python "%~dp0\BackhaulExtractor.py"  --noheadless --SelectTime specific_to_date 2022-03-08 2022-03-08
rem python "%~dp0\BackhaulExtractor.py" --noheadless --SelectTime relative 48
rem python "%~dp0\BackhaulExtractor.py" --noheadless --SelectTime specific_to_date daysago 5

rem Get the Week Day
for /f %%i in ('powershell ^(get-date^).DayOfWeek') do set dow=%%i

rem Decision point in case the current day is Monday
if %dow% == Monday (goto dMonday) else (goto dOther)

rem If today is Monday, run the code up to 3 days ago
:dMonday
for /l %%x in (1, 1, 3) do (
   python "%~dp0\BackhaulExtractor.py" --noheadless --SelectTime specific_to_date daysago %%x
   d:\lar_vivonpm_svn\tools\selenium\backhaul\scripts\run_preprocessing.bat
)
goto continue

rem If today is NOT Monday, run the code up to 1 day ago
:dOther
for /l %%x in (1, 1, 1) do (
   python "%~dp0\BackhaulExtractor.py" --noheadless --SelectTime specific_to_date daysago %%x
)
goto continue

rem run the additional data processing in BacoDB
:continue
rem d:\lar_vivonpm_svn\tools\selenium\backhaul\scripts\run_preprocessing.bat

rem HELP

rem FOR statement
rem (inicio, step, fim)

pause