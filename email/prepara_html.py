import os, subprocess, time, winsound
from ahk import AHK

path_e2e = 'C:\\Users\\jwx903529\\Documents\\Repositorio_NPM\\Repositorio\\e2e\\'

dash = path_e2e+'reports\\dashboard_hourly_v2.twb'
path_email_html = path_e2e+'tools\\Selenium\\email\\'
map_tableau = path_email_html+'MapeamentoTableau\\'

mouse_positions = {

'scroll_right_dashboard':(1287, 754),
'scroll_left_dashboard':(1238, 755),
'dashboard_menu':(242, 32),
'export_image':(301, 211),
'insert_exportpath':(901, 47),
'salvar_button':(1193, 737),
'insert_senha_BD':(763, 331),
'sign_in':(1288, 731),
'sair':(1348, 6)

}

dashboards_positions={

'MVNO_Dash':(330, 732),
'Roaming_Dash':(428, 731)

}

ahk = AHK()

def clica(mouse_pos):
    ahk.mouse_move(mouse_pos[0],mouse_pos[1],speed=10)
    ahk.click()

def desabilita_monitor2():
    ahk.send_input('#P')
    time.sleep(4)
    clica((252, 100))
    time.sleep(1)
    clica((689, 378))


def habilita_monitor2():
    ahk.send_input('#P')
    time.sleep(4)
    clica((317, 310))
    time.sleep(1)
    clica((689, 378))

def pos_imagem(imagem):
    posi = ahk.image_search(map_tableau+imagem)
    if posi:
        x0 = posi[0]
        y0 = posi[1]
        coord = (x0, y0)
    else:
        print("Imagem encontra-se fechada ou obstruída")
        coord = None
    return coord

def espera_imagem(imagem):
    while not pos_imagem(imagem):
        time.sleep(1)
        print('Esperando por imagem '+imagem)

def executa_bat(bat):
    #print("Executando isso:"+"\n"+bat)
    with open("temp.bat",'w') as script:
        script.write(bat)
    subprocess.call([path_email_html+'temp.bat'],shell=True)

def inicia_tableau(dash):
    inicia_tableau = """
    set PATH=%PATH%;C:\\Program Files\\Tableau\\Tableau 2019.4\\bin\\;
    tableau <twb>
    """
    inicia_tableau = inicia_tableau.replace('<twb>', dash)
    executa_bat(inicia_tableau)

def limpa_imagens():
    limpa_casa= "DEL /F/Q/S \"C:\\Users\\jwx903529\\Documents\\Repositorio_NPM\\Repositorio\\e2e\\tools\\Selenium\\email\\pagina_html\\Dashs\\\" > NUL"
    executa_bat(limpa_casa)

def expect_login():
    #print('\n','Aguardando tela de login...')
    while True:
        if ahk.pixel_get_color(1288, 731) == '0xEB8F50':
            #print("tela de login detectada!")
            break
        else:
            pass

def espera_login_maximizado():
    while not ahk.find_window(title=b'Sign in to reconnect'):
        "wait"
    win_dash = ahk.find_window(title=b'Tableau - dashboard_hourly_v2')
    win_sign = ahk.find_window(title=b'Sign in to reconnect')
    win_dash.maximize()
    win_sign.maximize()
    win_sign.activate()


def faz_login():
    expect_login()
    ahk.type("huawei1234")
    clica(mouse_positions['sign_in'])
    time.sleep(10)

desabilita_monitor2()
inicia_tableau(dash)
espera_login_maximizado()
expect_login()
faz_login()
