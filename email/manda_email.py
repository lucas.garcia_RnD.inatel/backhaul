# Send an HTML email with an embedded image and a plain text message for
# email clients that don't want to display the HTML.

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.header import Header
from datetime import date
import smtplib

path_html = 'C:\\Users\\jwx903529\\Documents\\Repositorio_NPM\\Repositorio\\e2e\\tools\\Selenium\\email\\pagina_html\\'
nome_arquivo = 'email.html'
#titulo = 'NPM Vivo | Early Morning | Core PS/CS/IMS | '+date.today().strftime("%d/%m/%Y")
titulo = 'NPM Vivo | Day After Report | Core PS/CS/IMS | '+date.today().strftime("%d/%m/%Y")

ok = input('Você está mandando um e-mail com o seguinte título: ' + titulo)

# Define these once',' use them twice!
#FROM = "joao.neto.myb@huawei.com"
FROM = "thiago.matsui@huawei.com"
#TO = ['lucas.jaksys.myb@huawei.com']
#CC = ['thiago.matsui@huawei.com', 'joao.neto.myb@huawei.com']

CC =['joao.neto.myb@huawei.com','thiago.matsui@huawei.com','lucas.jaksys.myb@huawei.com','romulo.torres@huawei.com',
'zhangjiejie.zhangjie@huawei.com','baker.yangxiao@huawei.com','otavio.garrido@huawei.com','adalbertot.almeida@huawei.com',
'leonardo.rbastos@huawei.com','yamaguchi.breno@huawei.com','alexandre.wagner@huawei.com','marcio.batista@huawei.com',
'fabio.puntar.myb@huawei.com','thiago.matsui@huawei.com','felipe.luccas.myb@huawei.com','rafael.vieira@huawei.com',
'fabiano.ferreira@huawei.com','jorge.amin.randstad@huawei.com','marcelo.fvgomes@huawei.com','oswaldo.silva@huawei.com',
'rodrigo.bastos@huawei.com','guilherme.miguel.manpower@huawei.com']

TO = ['pauloh.regis@telefonica.com','adilson.guimaraes@telefonica.com','antonio.jrodrigues@telefonica.com',
'paulo.martini@telefonica.com','talita.melo@telefonica.com','andre.ferrerreiz@telefonica.com','leonel.castro@telefonica.com',
'esthefani.godoi@telefonica.com','sandro.modenese@telefonica.com','alexandre.pinto@telefonica.com','fernando.coutinho@telefonica.com',
'fabio.degrecci@telefonica.com', 'douglas.kawamoto@telefonica.com','Eduardo.Santos4@telefonica.com', 'breno.cotta@telefonica.com',
'matheus.pinto@telefonica.com', 'livia.gerk@telefonica.com', 'alex.ysano@telefonica.com']

SERVER = "imailbr.huawei.com"

def html_to_string(path_html, file):
    with open(path_html+file, 'r') as html:
        html_string = html.read()
    return html_string

def paths_to_dict(html_string):
    #---Carrega o HTML e extrai os paths das imagens criando um dict = {'image_number': 'path'}
    dict_img = {}
    i=0
    while html_string.find('src')!=-1:
        i+=1
        dict_img['image'+str(i)] = html_string[html_string.find('src'):html_string.find('.png')+5]
        #---substitui os 'path' por 'cid:image_number'
        html_string=html_string.replace(dict_img['image'+str(i)], r'SRC="'+'cid:'+'image'+str(i)+r'"')
        dict_img['image'+str(i)] = dict_img['image'+str(i)].strip(r'src=').strip(r'"').replace('/','\\')

    return html_string, dict_img

def add_imagens(dict_img, path_html):
    # This example assumes the image is in the current directory
    for image in dict_img:
        with open(path_html+dict_img[image], 'rb') as i:
            msgImage = MIMEImage(i.read())

        # Define the image's ID as referenced above
        msgImage.add_header('Content-ID', '<'+image+'>')
        msgRoot.attach(msgImage)

def safe_str(obj):
    return obj.encode('ascii', 'ignore').decode('ascii')

# Create the root message and fill in the from, to, and subject headers
msgRoot = MIMEMultipart('related')
#msgRoot['Subject'] = 'SQM Vivo | Daily Report | Area 21 | 22/06/2020'
msgRoot['Subject'] = titulo
msgRoot['From'] = FROM
msgRoot['To'] = ", ".join(TO)
msgRoot['Cc'] = ", ".join(CC)

# Encapsulate the plain and HTML versions of the message body in an
# 'alternative' part, so message agents can decide which they want to display.
msgAlternative = MIMEMultipart('alternative')
msgRoot.attach(msgAlternative)
msgText = MIMEText('This is the alternative plain text message.')
msgAlternative.attach(msgText)


# We reference the image in the IMG SRC attribute by the ID we give it below
html_string = html_to_string(path_html, nome_arquivo)
html_string, dict_img = paths_to_dict(html_string)
msgText = MIMEText(html_string, 'html', 'utf-8')
#msgText['Subject'] = Header(safe_str(html_string), 'utf-8')
#msgRoot.attach(msgText)
msgAlternative.attach(msgText)
add_imagens(dict_img, path_html)

# Send the mail
server = smtplib.SMTP(SERVER)
#server.login('jwx903529', 'Jarvisativar#1612')
server.login('t00715274', 'MatLeftKneeTwice1315/')
server.sendmail(FROM, TO+CC, msgRoot.as_string())
server.quit()
print('e-mail enviado!')
