#######MODULOS BASICOS######
import time, os, sys
import argparse
import datetime
from datetime import date
import pandas
import yaml
from shutil import copyfile
#######MODULOS AUTOMAÇÂO WEB#########
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import ElementClickInterceptedException, NoSuchElementException
from bs4 import BeautifulSoup

def load_yaml():
    with open('config_HPM.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data
config_HPM = load_yaml()

#Roda a porra toda em background
headless = False

#Login Huawei PM
username = config_HPM['HUAWEIPM']['username']
senha = config_HPM['HUAWEIPM']['senha']

#Nome de colunas que identificam os elementos
NE = config_HPM['HUAWEIPM']['NE']

#######SUBSTITUA SEUS PATHS AQUI#######
#pasta onde o seu navegador faz os downloads dos csvs
path_to_downloads = config_HPM['PATHS']['path_to_downloads']
#pasta para onde esse script vai armazenar esses csvs
path_arquivos_dest = config_HPM['PATHS']['path_arquivos_dest']
#logs de envio
path_to_logs = config_HPM['PATHS']['path_to_logs']

#######XPATHS#######
templates_h = config_HPM['HUAWEIPM']['TemplatesXpaths']['templates_h']

templates_w = config_HPM['HUAWEIPM']['TemplatesXpaths']['templates_w']

templates_h_e_s = dict(templates_h,**templates_w)


templates = templates_w

#######################FUNÇÔES DE AUTOMAÇÂO COM SELENIUM########################
def espera_clica(xpath):
    while True:
        try:
            wait = WebDriverWait(driver,20)
            quit_queryperformance = wait.until(EC.element_to_be_clickable((By.XPATH, xpath)))
            quit_queryperformance.click()
            print('achou '+ xpath)
            break
        except ElementClickInterceptedException:
            time.sleep(1)
            print('esperando '+ xpath +' aparecer...')


def espera(xpath):
    print('esperando...')
    while True:
        try:
            wait = WebDriverWait(driver,200)
            quit_queryperformance = wait.until(EC.visibility_of_element_located((By.XPATH, xpath)))
            print('achou '+ xpath)
            break
        except:
            time.sleep(1)
            print('esperando '+ xpath +' aparecer...')

def espera_download_comecar(path_to_downloads,n_files):
    while len(os.listdir(path_to_downloads)) == n_files:
        pass
    print('Download iniciado!')


def espera_download(path_to_downloads):
    seconds = 0
    dl_wait = True
    while dl_wait and seconds < 80:
        time.sleep(1)
        dl_wait = False
        for fname in os.listdir(path_to_downloads):
            if fname.endswith('.crdownload'):
                dl_wait = True
        seconds += 1
    print('Download terminado!')
    return seconds

def download():
    espera_clica('//*[@id="pageDownloadVersion_combox_span"]')

    dl_format = driver.find_element_by_xpath('//*[@id="_0_Csv(*.csv)_csv"]')
    dl_format.click()
    time.sleep(1)

    espera_clica(r'//*[@id="pageDownload_btn_label"]')

def seleciona_elementos(nome_template):
    print('selecionando elementos...')
    if nome_template.split('_')[0] in config_HPM['HUAWEIPM']['SelectElements']:
        lista_xpaths = config_HPM['HUAWEIPM']['SelectElements'][nome_template.split('_')[0]]['lista_xpaths']
    else:
        lista_xpaths = config_HPM['HUAWEIPM']['SelectElements']['DEFAULT']['lista_xpaths']
    for xpath in lista_xpaths:
        espera_clica(xpath)

def identifica_arquivos(path_to_downloads, gran):
    if gran == 's':
        gran = '_W_'
    else:
        gran = '_H_'
    nomes = [name.split('_')[0] for name in templates]
    data = str(date.today().strftime("%Y%m%d"))
    lista = os.listdir(path_to_downloads)
    nome_arquivos= {}
    for arquivo in lista:
        for nome in nomes:
            if nome in arquivo and data in arquivo and gran in arquivo:
                nome_arquivos[nome] = arquivo
    return nome_arquivos

def apaga_arquivos(lista_arquivos):
    for arquivo in lista_arquivos:
        os.remove(path_to_downloads+lista_arquivos[arquivo])
        print('Arquivo '+lista_arquivos[arquivo]+' removido!')

def copia_arquivo(nome_arquivos):
    for nome in nome_arquivos:
        copyfile(path_to_downloads+nome_arquivos[nome], path_arquivos_dest+nome_arquivos[nome])
        print('Arquivo '+ nome_arquivos[nome] + ' movido para '+ path_arquivos_dest)


def scrap_table(html):
    soup = BeautifulSoup(html)

    data = []
    #table = soup.find('table', attrs={'class':'lineItemsTable'})
    table_body = soup.find('tbody')

    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols if ele]) # Get rid of empty values
    return data

################################################################################
#EXECUTA AUTOMAÇÂO WEB VIA SELENIUM_SERVER
################################################################################


########FODA-SE SSL/TSL#################
desired_capabilities = DesiredCapabilities.CHROME.copy()
desired_capabilities['acceptInsecureCerts'] = True
options = webdriver.ChromeOptions()
options.add_argument('--start-maximized') #

if headless:
    options.add_argument("--headless") # Runs Chrome in headless mode.
    options.add_argument('--no-sandbox') # Bypass OS security mode
    options.add_argument('--disable-gpu')  # applicable to windows os only


###########INICIA WEBDRIVER#############
driver = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=desired_capabilities,options=options)

driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': path_to_downloads}}
command_result = driver.execute("send_command", params)

driver.get("https://187.100.225.213:15200/AcrossPM-Web/")


##########FazLogin##########
#    try:
element = driver.find_element_by_id("username")
element.clear()
element.send_keys(username)
element = driver.find_element_by_id("password")
element.clear()
element.send_keys(senha,Keys.RETURN)
time.sleep(4)
number = 0
#    except NoSuchElementException:
#        print("Sai do SPES sua anta!")
#        driver.quit()
#        sys.exit()

#for template in templates:

#####################performanceQuery########################
iframes = driver.find_elements_by_tag_name("iframe")
driver.switch_to.frame(iframes[0])
actions = ActionChains(driver)
menu = driver.find_element_by_id("instantQuery")
actions.move_to_element(menu).perform()
queryperformance = driver.find_element_by_xpath(r'//*[@id="queryTemplateConfig"]')
queryperformance.click()
iframes = driver.find_elements_by_tag_name("iframe")
driver.switch_to.frame(iframes[2])

#espera_clica(r'//*[@id="toolbar-4"]')

espera_clica(r"//*[contains(text(), '_TRIALS')]/../../span")

espera_clica(r"//*[contains(text(), '00NPM_HW')]/../../span")

espera(templates['ATS_H'])

driver.find_element_by_xpath('//*').send_keys(Keys.PAGE_DOWN)

dict_indicadores = {}

for template in templates:
    espera_clica(templates[template])
    espera_clica(r'/html/body/div[1]/div[2]/div[2]/div/div/div/div[1]/div[4]/div[2]/div/div/div[1]/div/div[1]')
    time.sleep(0.5)
    espera_clica(r'/html/body/div[1]/div[2]/div[2]/div/div/div/div[1]/div[1]/ul/li[1]/a/span')
    time.sleep(0.5)
    espera_clica(r'//div[2]/div/div[1]/ul/li[2]/div/span[2]')
    time.sleep(0.5)
    espera('//div[2]/div/div[2]/div/div[3]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[1]/table')
    time.sleep(8)

    element = driver.find_element_by_xpath('//div[2]/div/div[2]/div/div[3]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[1]/table')
    html_table = element.get_attribute('innerHTML')

    data = scrap_table(html_table)

    dict_indicadores[template] = data

    sair = driver.find_element_by_class_name('window-title-close')
    sair.click()

    print(data)

novo_dict = {}
for tipo_elem in dict_indicadores:
    lista = [x[0] for x in dict_indicadores[tipo_elem]]
    novo_dict[tipo_elem] = lista
    with open(tipo_elem+'results.txt', 'a') as arquivo:
        for kpi in novo_dict[tipo_elem]:
            arquivo.write(kpi+'\n')
